This module disables/deletes unnecessary commerce discount rules to combat site slowdowns and reduce clutter.

It was created in something of a hurry, and was designed specifically for the target site.  Its general-purpose utility is unknown, and it is offered simply in case it helps someone.  Please carefully evaluate on a testing site before even considering use on a production site.

Cleanup can be executed in three ways:
    - cron:  there are currently no persistent settings, so turn on cleanup at cron by manually editing the code and enabling the call in the hook_cron implementation
    - drush:  commerce-discount-rule-cleanup [max_num_rules] [examine_only] [force_delete] [log_path]  (drush alias is cdrc)
    - admin ui:  admin/commerce/config/commerce_discount_rule_cleanup

"Max num rules" is the maximum number of rules to process
"Examine only" means everything executes as normal, but nothing is actually disabled or deleted.
"Force delete" means that if a rule would normally be disabled, it is deleted instead.
"Log path" is the path to a log file that records the names of any rules that were actioned.

Note on "Max num rules":  this is the number of rules *processed*, that is deleted/disabled, not the number of rules examined.  It is useful if you have a lot of rules to delete, because deleting can be slow and without limiting you can get timeouts and errors.
