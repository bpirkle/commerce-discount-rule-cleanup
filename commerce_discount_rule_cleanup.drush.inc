<?php


/*
 * Our custom drush commands.
 *
 * "callback arguments" (which are hard-coded) will appear before
 * any "arguments" (which are traditional command line).  We tend to
 * use "callback arguments" to pass a flag indicating that the callback
 * function was called from drush.  That's because we sometimes also
 * call these functions from other code, and we may want to output
 * status/error messages differently if called from drush.
 *
 * Decent refrence on creating custom drush commands here:
 * http://drush.ws/docs/commands.html
 */
function commerce_discount_rule_cleanup_drush_command() {
  $items  = array();

  $items['commerce-discount-rule-cleanup'] = array(
    'aliases' => array('cdrc'),
    'callback' => 'drush_commerce_discount_rule_cleanup_cleanup_rules',
//    'callback arguments' => array(TRUE),
    'description' => dt('Pricing Rule Cleanup'),
    'arguments' => array(
      'max_num_rules' => "Maximum number of rules to process (disable/delete) in one operation.",
      'examine_only' => "Examine rules but don't actually disable/delete anything. Useful for gathering status.",
      'force_delete' => "always delete rules (instead of disabling).  Useful for more aggressive cleanup.",
      'log_path' => "Path for detailed activity logging, or empty for none.",
    ),
  );

  return $items;
}


function commerce_discount_rule_cleanup_drush_help($section) {
  switch ($section) {
    case 'commerce_discount_rule_cleanup':
      return dt("Cleanup of pricing rules.  [max_num_rules] [examine_only] [force_delete] [log_path].");
  }
}


function drush_commerce_discount_rule_cleanup_cleanup_rules($max_num_rules, $examine_only, $force_delete, $log_path = '') {
  $msg = commerce_discount_rule_cleanup_cleanup_rules($max_num_rules, $examine_only, $force_delete, $log_path);
  drush_print($msg);
}

